# Cuisines Registry

## Java Implementation:

This task is implemented using Java 8 as i have used Streams and lambda , so kindly make sure that to run it into Java 8 or above. Also Gradle constrain is added and it will compile using lower version of Java.

## How to scale:
The enhancement i have made for this task is to use hash map and override hashcode and equals methods to enable faster storage access, and to scale this for more records we can use one of the below options
1. Use in memory data base like H2 and implement the methods to be SQL based lookup.
2. Use any distributed caching third lib like eHcache or Redis
