package de.quandoo.recruitment.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	/**
	 * Hash Map to store the cuisines and customers for faster data access, 
	 * the key will be the cuisine and value will be list of customers registered for that Cuisine.
	 * */
	private static Map<Cuisine, List<Customer>> cuisinesRegistry = new HashMap<>();
	/***
	 * List of allowed CUISINES
	 */
	private static final List<String> ALLOWED_CUISINES = Arrays.asList("italian", "french", "german");

	/***
	 * Register new Customer with cuisine, if not exist then create new one.
	 * It checks if cuisine is one of the allowed categories otherwise it prints error and returns.
	 * @param userId the user we want to register.
	 * @param cuisine the cuisine we want to register user to 
	 */
	@Override
	public void register(final Customer userId, final Cuisine cuisine) {
		

		if (userId == null || cuisine == null) {
			System.err.println("Can't Register Null values");
		}
		String name = cuisine.getName();
		if (name == null || name.isEmpty() || !ALLOWED_CUISINES.contains(name.toLowerCase())) {
			System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
		}
		List<Customer> cuisineCustomers = cuisineCustomers(cuisine);
		if (cuisineCustomers == null) {
			cuisineCustomers = new ArrayList<>();
		}
		// check if customer already registered
		if (cuisineCustomers.contains(userId)) {
			System.out.println("User Already Registered !");
		} else {
			cuisineCustomers.add(userId);
			cuisinesRegistry.put(cuisine, cuisineCustomers);
		}
	}

	/**
	 * This method return list of Customers registered for given cuisine.
	 * @param cuisine the cusinine we want it's customers.\
	 * @return List of Customers registered for given cuisine or null in case input is null or no customers registered for it.
	 */
	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		if (cuisine != null) {
			return cuisinesRegistry.get(cuisine);
		}
		return null;
	}

	/**
	 * This method return list of Cuisines for a given customer.
	 * @param customer the customer we want to get it's Cuisines.\
	 * @return List of Cuisines belongs to given customer or null in case input is null or no Cuisines registered for it.
	 */
	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		if (customer != null) {
			return cuisinesRegistry.entrySet()
					.stream()
					.parallel()// parallel loop of all entries of the registry
					.filter((cuisineCustomers) -> cuisineCustomers.getValue().contains(customer))// filter all entries
					.map(Map.Entry::getKey)// get the key list
					.distinct()// to remove duplicate
					.collect(Collectors.toList());// collect the result into list and return it
		}
		return null;
	}

	/**
	 * This method returns List of Cuisines where number of it's registered customers is greater than or equal the given input.
	 * @param n the customer we want to get it's Cuisines.\
	 * @return List of Cuisines with highest registered customers or empty list if input is less than or equal zero.
	 */
	@Override
	public List<Cuisine> topCuisines(final int n) {
		
		if (n<=0) {
			return new ArrayList<>();
		}
		
		return cuisinesRegistry.entrySet()
				.stream()
				.parallel()// parallel loop of all entries of the registry
				.filter((cuisineCustomers) -> cuisineCustomers.getValue().size()>=n)// filter all entries who's size >= n
				.map(Map.Entry::getKey)// get the key list
				.distinct()// to remove duplicate
				.collect(Collectors.toList());
	}

}
