package de.quandoo.recruitment.registry;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistryTest {

    private static InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
    
    /**
     * Do Data preparation to be available for all test cases . as there is no need to do it before each test case.
     */
    @BeforeClass
    public static void setup() {
    	cuisinesRegistry.register(new Customer("french1"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("german2"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("italian03"), new Cuisine("italian"));
    	cuisinesRegistry.register(new Customer("french11"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("german022"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("italian33"), new Cuisine("italian"));
    	cuisinesRegistry.register(new Customer("french111"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("german22"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("italian3"), new Cuisine("italian"));
    	
    	cuisinesRegistry.register(new Customer("german222"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("german2222"), new Cuisine("german"));
    	cuisinesRegistry.register(new Customer("french1111"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("french11"), new Cuisine("german"));
    }

    /**
     * this method is used to test GetCuisineCustomers
     */
    @Test
    public void testGetCuisineCustomers() {
       List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
       Assert.assertEquals(4, customers.size());
       customers = cuisinesRegistry.cuisineCustomers(new Cuisine("german"));
       Assert.assertEquals(6, customers.size());
       customers = cuisinesRegistry.cuisineCustomers(new Cuisine("italian"));
       Assert.assertEquals(3, customers.size());
    }

    /**
     * this method is used to test negative scenarios
     */
    @Test
    public void testNullInputs() {
        Assert.assertNull(cuisinesRegistry.cuisineCustomers(null));;
        Assert.assertNull(cuisinesRegistry.customerCuisines(null));;
    }

    /**
     * this method is used to test CustomerCuisines and make sure that 1 customer can be registered to multiple Cuisines
     */
    @Test
    public void testCustomerCuisines() {
    	
    	List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("german2"));
    	Assert.assertEquals(1, cuisines.size());
    	cuisines = cuisinesRegistry.customerCuisines(new Customer("french11"));
    	Assert.assertEquals(2, cuisines.size());
    }

    /**
     * this method is used to test get TopCuisines
     */
    @Test 
    public void testTopCuisines() {
       List<Cuisine> cuisines = cuisinesRegistry.topCuisines(4);
       Assert.assertEquals(2, cuisines.size());// German and frensh
       String cuisinesStr = cuisines.stream()
    		   						.map((c)->c.getName())
    		   						.collect(Collectors.joining(","));
       Assert.assertTrue(cuisinesStr.contains("german"));
       Assert.assertTrue(cuisinesStr.contains("french"));
       cuisines = cuisinesRegistry.topCuisines(5);
       Assert.assertEquals(1, cuisines.size());// German
       Assert.assertTrue(cuisines.get(0).getName().equalsIgnoreCase("german"));
    }


}